"use strict";

if (process.env.NODE_ENV == 'production')
	console.log("\x1b[31m", "You are now in production mode, every transaction are considered as REAL trasaction!");
else if (process.env.NODE_ENV == 'development')
	console.log("\x1b[32m", "You are now in cloud dev, environment!");
else
	console.log("All changes you made only applies locally, Have fun!");

const express= require('express')
, app= express()
, serverInit= async (app)=> {
	await require('./app/kernel/http')(app);
	await require('./app/kernel/validator')(app);
	await require('./app/kernel/security')(app);
	await require('./app/kernel/session')(app);
	await require('./app/kernel/render')(app, express);
	await require('./app/kernel/auth')(app);
	await require('./app/kernel/interceptor')(app);

	await require('./app/routes')(app);
	await require('./app/kernel/errorHandler')(app);
}

serverInit(app);

module.exports = app;
