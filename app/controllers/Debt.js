"use strict";

const models  = require(__dirname+'/../models')
, APIFormat  = require(__dirname+'/../helpers/APIFormat')
, methods = {

	index(req, res) {
		return res.render('test', {
			layout:'layouts/master',
		});
    },
    
    create(req, res) {
        //mobile-2.1 in here
		return res.render('test', {
			layout:'layouts/master',
		});
	},

    store(req, res) {
		return res.render('test', {
			layout:'layouts/master',
		});
	}
}

module.exports = methods;