"use strict";

const models  = require(__dirname+'/../models')
, APIFormat  = require(__dirname+'/../helpers/APIFormat')
, methods = {
    async index(req, res) {
        //screen mobile-1 here
		return res.render('pages/bills', {
			layout:'layouts/master',
		});
    },
    
	async detail(req, res) {
        //screen mobile-2 here
		req.assert('bill_id', 'Invalid bill id').isInt();
        
        var errors = req.validationErrors();
        
        if (errors.length>0) {
            return res.redirect('back');
        }

        const bill = await models.Bill.findById(req.params.bill_id);

        return res.json({bill});
	}
}

module.exports = methods;