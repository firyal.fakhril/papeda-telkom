"use strict";

const models  = require(__dirname+'/../models')
, APIFormat  = require(__dirname+'/../helpers/APIFormat')
, methods = {

	async index(req, res) {
        const users = await models.Users.findAll();
		// return res.json({ok: 'oce'})
		return res.json({users});
	}
}

module.exports = methods;