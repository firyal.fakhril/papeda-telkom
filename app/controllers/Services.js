"use strict";

const models  = require(__dirname+'/../models')
, APIFormat  = require(__dirname+'/../helpers/APIFormat')
, methods = {

	index(req, res) {
		// mobile 4
		return res.render('pages/services', {
			layout:'layouts/master',
		});
	},

	indihomeMap(req, res) {
		// mobile 4.1
		return res.render('test', {
			layout:'layouts/master',
		});
	},

	chatSupport(req, res) {
		// mobile 3
		return res.render('test', {
			layout:'layouts/master',
		});
	}
}

module.exports = methods;