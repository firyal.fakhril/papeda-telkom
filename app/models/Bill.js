"use strict";

module.exports = function(sequelize, DataTypes) {
    var Bill = sequelize.define("Bill", {
        bil_id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true},
        bil_user_id: { type: DataTypes.INTEGER(11).UNSIGNED },
        bil_title: { type: DataTypes.STRING, allowNull:true},
        bil_amount: { type: DataTypes.INTEGER(11).UNSIGNED }
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'bill',
        classMethods: {
            associate: function(models) {
                
            }
        }
    });

return Bill;
};
