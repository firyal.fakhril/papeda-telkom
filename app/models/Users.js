"use strict";
var bcrypt = require('bcryptjs');

var crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {
    var Users = sequelize.define("Users", {
        user_id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true},
        user_email: { type: DataTypes.STRING, allowNull:true},
        user_password: { type: DataTypes.STRING, allowNull:true},
        user_firstname: { type: DataTypes.STRING, allowNull:true},
        user_lastname: { type: DataTypes.STRING, allowNull:true}
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,        
        freezeTableName: true,
        tableName: 'users',
        instanceMethods: {
            validPassword: function (password) {
                return bcrypt.compareSync(password, this.user_password);
            },
            validOldPassword: function (password) {
                var sha1 = crypto.createHash('sha1').update(password).digest("hex");
                return (sha1 == this.user_password);
            }
        },
        classMethods: {
            associate: function(models) {
                
            }
        }
    });

var hasSecurePassword = function(user, options, callback) {
    bcrypt.hash(user.get('user_password'), 5, function(err, hash) {
        if (err) return callback(err);
        user.set('user_password', hash);
        return callback(null, options);
    });
};

Users.beforeCreate(function(user, options, callback) {
    if (user.user_email != null)
    {
        user.user_email = user.user_email.toLowerCase();

        if (user.user_password != null)
            hasSecurePassword(user, options, callback);
        else
            return callback(null, options);
    }
    else
        return callback(null, options);
});

Users.beforeUpdate(function(user, options, callback) {
    if (user.user_password!= null){
        hasSecurePassword(user, options, callback);
    }
    else
        return callback(null, options);
});

return Users;
};
