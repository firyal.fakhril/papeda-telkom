"use strict";

module.exports = function(sequelize, DataTypes) {
    var Debt = sequelize.define("Debt", {
        deb_id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true},
        deb_user_id: { type: DataTypes.INTEGER(11).UNSIGNED },
        deb_bill_id: { type: DataTypes.INTEGER(11).UNSIGNED },
        deb_amount: { type: DataTypes.INTEGER(11).UNSIGNED },
        deb_paid: { type: DataTypes.INTEGER(11).UNSIGNED }
    },
    {
        timestamps: true,
        paranoid: true,
        underscored: true,        
        freezeTableName: true,
        tableName: 'debt',
        classMethods: {
            associate: function(models) {
                
            }
        }
    });
    
    return Debt;
};
