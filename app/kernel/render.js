const path = require('path');
module.exports = (app, express) => {
	const viewConfig = require('../configs/'+((process.env.NODE_ENV==='local-dev')?'local-dev/':'')+'app')[process.env.NODE_ENV].views
	, expressLayouts = require('express-ejs-layouts')
	, path    = require("path");

	app.set('view engine', 'ejs');

	app.set('views', `.${viewConfig}`);
	app.use(expressLayouts);

	app.use(express.static(path.resolve(__dirname, './../../resources/public')));

}
