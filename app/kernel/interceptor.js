'use strict';

module.exports = (app) => {

	const host = require('../configs/'+((process.env.NODE_ENV==='local-dev')?'local-dev/':'')+'app')[process.env.NODE_ENV].hostURL;

	app.use( (req, res, next) => {
		req.baseurl = req.protocol + '://' + host; //get base url

		app.locals.alerts = {
			success: null,
			error: null
		}
		
		//intercept user and cart data
		app.locals.user = null;
		
		req.error = {
			message : 'Not found'
			, body : {}
			, status : 404
		}

		return next();
	});
}
