'use strict';

module.exports = (app) => {
	const APIFormat  = require(__dirname+'/../helpers/APIFormat')
	, dummy = (process.env.NODE_ENV!='production')

	app.use((req, res, next) => {
		
		if (req.error.status>=500) {
			console.error(req.error)
		}

		if (req.accepts("text/html")) {
			return res.render(`error/${req.error.status==404?'404':'500'}`, {
				layout: "layouts/master"
				, error: dummy?JSON.stringify(req.error):null
			})
		}
		else {
			return res.status(req.error.status).json( APIFormat.response(req.error.message, {}, ( (!dummy && (req.error.status>=500)) ? req.error.body : {})) )
		}

	})

	app.use((err, req, res, next) => {
		if(err.status>=500) {
			console.log('APP ERR :' , err, 'END OF APP ERR')
			console.log('APP ERR STACK:' , err.stack, 'END OF APP ERR STACK')
		}
		
		res.status(err.status || 500);
		res.render('error/500', {
			layout:'layouts/master',
			error: err.stack
		});
	});
}
