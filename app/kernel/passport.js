module.exports = () => {
    "use strict";
    const passport = require('passport')
    , LocalStrategy   = require('passport-local').Strategy
    , models  = require('./../models')
    , key = require('../configs/'+((process.env.NODE_ENV==='local-dev')?'local-dev/':'')+'app')[process.env.NODE_ENV].secret
    , jwt = require('jsonwebtoken');
    
    passport.serializeUser((user, done) => {
        done(null, user);
    });
    
    passport.deserializeUser((user, done) => {
        models.Users.findOne({
            where: {
                user_id: user.user_id
            }
            , attributes: [
                'user_id',
                'user_email',
                'user_firstname',
                'user_lastname'
            ]
        })
        .then(user => {
            var sessObj = {
                user_id: user.user_id,
                user_email: user.user_email,
                user_group: user.user_group,
                user_firstname: user.user_firstname,
                user_lastname: user.user_lastname,
            };
            
            var token = jwt.sign(sessObj, key);
            
            sessObj.apitoken = token;
            
            done(null, sessObj);
        })
        .catch(err => {
            done(err, null);
        });
    });
    
    passport.use('local-login', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true
    },
    (req, email, password, done) => {
        
        
        models.Users.findOne({
            where: {
                user_email: email
            },
            attributes: ['user_id', 'user_email', 'user_password']
        })
        .then(user => {
            
            if ( user==null || 
                !user.validPassword(password))
                return done(null, false, req.flash('error', 'Email dan/atau password salah.'));
                
                if (user.user_activated == 0)
                return done(null, false, req.flash('error', 'Akun belum aktif.'));
                
                var sessObj = {
                    user_id: user.user_id,
                    user_email: user.user_email
                }
                
                return done(null, sessObj);
                
            })
            .catch( err => {
                return done(err);
            });
        }));
        
        
        return passport;
    };
    