'use strict';

const RateLimit = require('express-rate-limit');

module.exports = (app) => {
	const secConfig = require('../configs/'+((process.env.NODE_ENV==='local-dev')?'local-dev/':'')+'security')[process.env.NODE_ENV]
	, lusca = require('lusca')

	app.use(lusca(secConfig));
	app.disable('x-powered-by');

	app.enable('trust proxy');

	var limiter = new RateLimit({
		windowMs: 1*60*1000,
		max: 150,
		delayMs: 0
	});

	// app.use(limiter);

	// app.use('/api/', limiter);
	// app.use('/checkout/', limiter);
	// app.use('/orders/', limiter);
	app.use('/login/', limiter);
	// app.use('/logout/', limiter);
	// app.use('/social-auth/', limiter);
	// app.use('/sociatop-ups/', limiter);
	app.use('/test/', limiter);
}
