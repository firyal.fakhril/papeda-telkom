"use strict";

const fs      = require("fs")
, path    = require("path")
, mdlwrs	= {};

fs
.readdirSync(__dirname)
.filter(function(file) {
	return (file.indexOf(".") !== 0) && (file !== "index.js");
})
.forEach(function(file) {
	mdlwrs[file.split(".")[0]] = require(path.join(__dirname, file));

});

module.exports = mdlwrs;