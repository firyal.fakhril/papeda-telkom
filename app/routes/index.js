"use strict";

const path = require("path")
, utils = require(`${process.env.PWD}/app/helpers/Utils`)
, controllers = require(`${process.env.PWD}/app/controllers`)
, middlewares = require(`${process.env.PWD}/app/middlewares`)

module.exports = async (app) => {
	const routeFiles= await utils.promisedFS(__dirname);

	try {
		routeFiles
		.map( filename => {
			if(filename !== "index.js") {
				return require(path.join(__dirname, filename));
			}
		})
		.reduce( (grouproutes, route) => {
			if(route != null){
				return grouproutes.concat(route)
			} 
			else{
				return grouproutes
			}
		}, [])
		.forEach( routeGroup => {
			let groupmiddlewares = ( Array.isArray(routeGroup.middlewares))?routeGroup.middlewares:[]
			, appliedGroupMdlwrs = [];

			if(process.env.NODE_ENV === 'local-dev') {
				console.log(`Route group with prefix ${routeGroup.prefix}`)
			}

			groupmiddlewares.forEach( (targetMiddleware) => {
				appliedGroupMdlwrs.push(utils.accessObjectPropertyByString(middlewares, targetMiddleware))
			})

			routeGroup.routes.forEach( (endpoints) => {
				let method = endpoints[0]
				, url = endpoints[1]
				, targetController = endpoints[2]
				, routeMiddlewares = ( Array.isArray(endpoints[3]))?endpoints[3]:[]
				, appliedMdlwrs = [];

				routeMiddlewares.forEach( (targetMiddleware) => {
					appliedMdlwrs.push(utils.accessObjectPropertyByString(middlewares, targetMiddleware))
				})

				if(process.env.NODE_ENV === 'local-dev') {
					console.log(`Route ${routeGroup.prefix+url} to ${targetController}`)
				}

				app[method](routeGroup.prefix+url, appliedGroupMdlwrs, appliedMdlwrs, utils.accessObjectPropertyByString(controllers, targetController));
			})
			if(process.env.NODE_ENV === 'local-dev') {
				console.log('')
			}
		})

	}
	catch(err) {
		console.error(err)
	}

}